var ss = SpreadsheetApp.openById("1zViigtDnXVIc5TTli50tzNDH1VOzG8hwj0pqaFe9gl0");
var sheet = ss.getSheetByName("แผ่น1");
 
function doPost(e) {
 
    var dialogflowDATA = JSON.parse(e.postData.contents);
//    var message = dialogflowDATA .queryResult.queryText;
    var intent = dialogflowDATA.queryResult.intent.displayName;
  
    var tableArray = sheet.getRange(1, 1, sheet.getLastRow(), sheet.getLastColumn()).getValues();
    var result = '';
    
    var Elast = ss.getRange("E1:E").getValues().filter(String).length + 1;
    var Glast = ss.getRange("G1:G").getValues().filter(String).length + 1;
    var Ilast = ss.getRange("I1:I").getValues().filter(String).length + 1;
    var Dlast = ss.getRange("D1:D").getValues().filter(String).length + 1;
    var Flast = ss.getRange("F1:F").getValues().filter(String).length + 1;
    var Hlast = ss.getRange("H1:H").getValues().filter(String).length + 1;
    var time = new Date();
    
    if (intent == sheet.getRange(2, 1).getValue()){
            sheet.getRange('E' + Elast).setValue(parseInt(sheet.getRange(2, 2).getValue())+" คน");
            sheet.getRange('D' + Dlast).setValue(time);
    }
    if (intent == sheet.getRange(3, 1).getValue()){
            sheet.getRange('G' + Glast).setValue(parseInt(sheet.getRange(3, 2).getValue())+" คน");
            sheet.getRange('F' + Flast).setValue(time);
    }
    if (intent == sheet.getRange(4, 1).getValue()){
            sheet.getRange('I' + Ilast).setValue(parseInt(sheet.getRange(4, 2).getValue())+" คน");
            sheet.getRange('H' + Hlast).setValue(time);
    }
    
    for (var i = 0; i < tableArray.length; i++) {
 
        if (tableArray[i][0] == intent) {
            i = i + 1;
            var result = {
                "fulfillmentMessages": [{
                    "platform": "line",
                    "type": 4,
                    "payload": {
                        "line": {
                       
  "type": "flex",
  "altText": "Flex Message",
  "contents": {
    "type": "bubble",
    "direction": "ltr",
    "header": {
      "type": "box",
      "layout": "vertical",
      "contents": [
        {
          "type": "text",
          "text": intent,
          "align": "center"
        }
      ]
    },
    "hero": {
      "type": "image",
      "url": "https://medias.thansettakij.com/images/2020/02/12/1581487955_1.jpeg",
      "size": "full",
      "aspectRatio": "1.51:1",
      "aspectMode": "fit"
    },
    "body": {
      "type": "box",
      "layout": "vertical",
      "contents": [
        {
          "type": "text",
          "text": parseInt(sheet.getRange(i, 2).getValue())+" คน",
          "align": "center"
        }
      ]
    }
  }

                        }
                    }
                }]
            }
 
            var replyJSON = ContentService.createTextOutput(JSON.stringify(result)).setMimeType(ContentService.MimeType.JSON);
            return replyJSON;
        }
    }
}